import logo from './logo.svg';
import './App.css';
import React from 'react';
import WeatherPerWeek from './components/WeatherPerWeek';
import MapWeatherForecast from './components/MapWeatherForecast';
import LocationPhoto from './components/LocationPhoto';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,  
  useLocation
} from "react-router-dom";
import moment from 'moment'


function App() {
  return (
    <Router>
    <div>
        <nav>
          <ul class="nav">
            <li>
              <Link to="/location">Location Photo</Link>
            </li>
            <li>
              <Link to="/weathermap">Maps of weather</Link>
            </li> ниже в switch <Route path="/weathermap">
            <MapWeatherForecast />
            </Route> 
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/weatherperweek">Weather per week</Link>
            </li>
          </ul>
        </nav>

        <div className="App">
         
         <section> 
            <Switch>
              <Route path="/location">
                <LocationPhoto />
              </Route>

              <Route path="/weatherperweek/:id">
                <WeatherPerWeek />

              </Route>
              <Route path="/weatherperweek">
                <Redirect to={`/weatherperweek/${moment().format('YYYY-MM-DD')}`} />
              </Route>
              
              
              <Route path="/">
                <h1>Home</h1>
                Welcome to our service.<Link to="/location">WeatherForecast</Link> You can explore <Link to="/weathermap">text</Link>weather per week </Link>  
              </Route>
  
              <Route path="*">
                <NoMatch />
              </Route>
             
           </Switch>
           
 
         </section>
       </div>


      </div>
      </Router>
  );
}

function NoMatch() {
  let location = useLocation();

  return (
    <div>
      <h3>
        No match for <code>{location.pathname}</code>
      </h3>
    </div>
  );
}

export default App;
